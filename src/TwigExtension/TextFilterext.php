<?php
namespace Drupal\textfilter\TwigExtension;

class TextFilterext extends \Twig_Extension {
    public function getFilters()
    {
        return [ new \Twig_SimpleFilter('customf', array($this, 'customFilter'))];
    }
    public function getName()
    {
        return 'textfilter.twig_extension';
    }
    public static function customFilter($str)
    {
        $string_data = preg_replace('/[^a-zA-Z0-9\s+]/','',$str);
        return $string_data;
    }
}
